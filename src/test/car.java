package test;

public class car {
	private double odometer;
    private String brand;
    private String color;
    private int gear;
   
    public car() {
        odometer = 0.;
        brand = "Renault";
        color = "White";
        gear = 0;
    }

    public car(String b, String c) {
        odometer = 0.;
        brand = b;
        color = c;
        gear = 0;
    }

    public car(String b) {
        odometer = 0.;
        brand = b;
        color = "White";
        gear = 0;
    }

   

    public car(String b, String c, ) {
        odometer = 0.;
        brand = b;
        color = c;
        gear = 0;
    }
public void incrementGear () {
	
	if( gear<5) {
		gear+=1;
	}else {
		System.out.println("Error: the gear value must be in range [0, 5].");
	}
}
public void decrementGear() {
	if( gear>5) {
		gear-=1;
	}else {
		System.out.println("Error: the gear value must be in range [0, 5].");
	}
}
public void drive(double numberOfHoursTravel,double kmperHour) {
	odometer+=numberOfHoursTravel*kmperHour;
	
}
public double getOdometer() {
    return odometer;
}

public String getBrand() {
    return brand;
}

public String getColor() {
    return color;
}

public void setColor(String c) {
    color = c;
}

public int getGear() {
    return gear;
}
public void display() {
	System.out.println("The " +brand+" car has color "+color+"."
			+ "It has travelled "+odometer+ "km so far."
			+"It is at gear "+gear+".");
}


}
