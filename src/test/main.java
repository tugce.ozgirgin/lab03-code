package test;

public class main {

	public static void main(String[] args) {
		car car1= new car("Mercedes","Yellow");
		car car2= new car("Ferrari", "Red");
		car car3= new car("Volkswagen", "Pink");
		
		car1.display();
        car2.display();
        car3.display();

		
		
		
		car1.drive(6.3,100);
		car1.incrementGear();
		car1.display();
		
		car2.drive(4.8, 100);
        car2.incrementGear();
        car2.incrementGear();
        car2.decrementGear();
        car2.incrementGear();
        car2.display();


	}

}
